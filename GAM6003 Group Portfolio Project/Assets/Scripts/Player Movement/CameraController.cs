using UnityEngine;

public class CameraController : MonoBehaviour
{
    /*Brackeys, 2019. FIRST PERSON MOVEMENT in Unity - FPS Controller. [Online] 
      Available at: https://www.youtube.com/watch?v=_QajrabyTJc
      [Accessed 04 10 2021].*/

    //Serialized Private Variables
    [Header("Controller Settings")]
    [SerializeField] private float mouseSensitivity;
    [Header("Script Attributes")]
    [SerializeField] private Transform player;
    //Private Variables
    private float xRotation;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        xRotation = 0.0f;
    }

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90.0f, 90.0f);

        transform.localRotation = Quaternion.Euler(xRotation, 0.0f, 0.0f);
        player.Rotate(Vector3.up * mouseX);
    }
}
