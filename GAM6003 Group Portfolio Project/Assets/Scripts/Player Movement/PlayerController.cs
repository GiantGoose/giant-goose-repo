using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /*Brackeys, 2019. FIRST PERSON MOVEMENT in Unity - FPS Controller. [Online] 
      Available at: https://www.youtube.com/watch?v=_QajrabyTJc
      [Accessed 04 10 2021].*/

    //Serialized Private Variables
    [Header("Controller Settings")]
    [SerializeField] private float speed;
    [SerializeField] private float jumpHeight;
    [SerializeField] private float gravity;
    [SerializeField] private float groundDist;
    [SerializeField] private float DashSpeed;
    [Header("Script Attributes")]
    [SerializeField] private CharacterController controller;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundMask;
    //Private Variables
    private Vector3 velocity;
    private bool isGrounded;

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDist, groundMask);

        if (isGrounded && velocity.y < 0.0f) velocity.y = -2.0f;

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 movement = transform.right * x + transform.forward * z;

        controller.Move(movement * speed * Time.deltaTime);

        if (Input.GetButtonDown("Jump") && isGrounded) velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }
}
