using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyCardUI : MonoBehaviour
{
    [SerializeField] public Key GreenCard, RedCard, PurpleCard;
    [SerializeField] RawImage Green, Red, Purple;
    // Start is called before the first frame update
    void Start()
    {
        Key GreenCard = gameObject.GetComponent<Key>();
        Key RedCard = gameObject.GetComponent<Key>();
        Key PurpleCard = gameObject.GetComponent<Key>();
        Green.enabled = false;
        Red.enabled = false;
        Purple.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        GreenKey();
        RedKey();
        PurpleKey();
    }

    void GreenKey()
    {
        if (GreenCard.haveKey == true)
        {
            Green.enabled = true;
        }
        else
        {
            Green.enabled = false;
        }
    }

    void RedKey()
    {
        if (RedCard.haveKey == true)
        {
            Red.enabled = true;
        }
        else
        {
            Red.enabled = false;
        }
    }

    void PurpleKey()
    {
        if (PurpleCard.haveKey == true)
        {
            Purple.enabled = true;
        }
        else
        {
            Purple.enabled = false;
        }
    }

}
