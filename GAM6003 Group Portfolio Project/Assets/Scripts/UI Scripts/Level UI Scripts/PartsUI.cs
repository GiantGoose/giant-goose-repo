using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartsUI : MonoBehaviour
{
    [SerializeField] MachineConsole MC;
    [SerializeField]RawImage Core, GreyScaleCore, TeslaCoil, GreyTeslaCoil, LeviPlate, GreyLeviPlate;
    public MachinePartsCollection mpc1, mpc2, mpc3;
    // Start is called before the first frame update
    void Start()
    {
        Core.enabled = false;
        TeslaCoil.enabled = false;
        LeviPlate.enabled = false;
        MachinePartsCollection mpc1 = gameObject.GetComponent<MachinePartsCollection>();
        MachinePartsCollection mpc2 = gameObject.GetComponent<MachinePartsCollection>();
        MachinePartsCollection mpc3 = gameObject.GetComponent<MachinePartsCollection>();
    }

    // Update is called once per frame
    void Update()
    {
        Coil();
        MCore();
        Levi();
        if (MC.MachineParts == 0 && mpc1.hasPart == false)
        {
            Core.enabled = false;
            TeslaCoil.enabled = false;
            LeviPlate.enabled = false;
        }
    }

    void Coil()
    {
        if (MC.MachineParts == 1 || mpc1.hasPart == true)
        {
            TeslaCoil.enabled = true;
            GreyTeslaCoil.enabled = false;
        }
    }

    void MCore()
    {
        if (MC.MachineParts == 2 || mpc2.hasPart == true)
        {
            Core.enabled = true;
            GreyScaleCore.enabled = false;
            TeslaCoil.enabled = true;
            //GreyTeslaCoil.enabled = false;
        }
        else
        {
            Core.enabled = false;
            GreyScaleCore.enabled = true;
        }
    }

    void Levi()
    {
        if (MC.MachineParts == 3 || mpc3.hasPart == true)
        {
            LeviPlate.enabled = true;
            GreyLeviPlate.enabled = false;
            TeslaCoil.enabled = true;
            //GreyTeslaCoil.enabled = false;
            Core.enabled = true;
            //GreyScaleCore.enabled = false;
        }
        else
        {
            LeviPlate.enabled = false;
            GreyLeviPlate.enabled = true; 
        }
    }
}
