using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;




public class UItextTriggerScript : MonoBehaviour
{
    //[SerializeField] private Text UIText;
    [SerializeField] private string UIMessage;
    //[SerializeField] private TMPro.TMP_Text UITEXT;
    [SerializeField] private TextMeshProUGUI UIText;
    [SerializeField] private bool PlayerLock;
    [SerializeField] CharacterController controller;

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            UIText.text = UIMessage;
            //UITEXT.text = UIMessage;
            if (PlayerLock == true)
            {
                controller.enabled = false;
                Invoke("ReleasePlayer", 2.0f);
            }
        }
    }
    private void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            UIText.text = null;
            //UITEXT.text = null;
        }
    }

    void ReleasePlayer()
    {
        controller.enabled = true;
    }
}
