using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashUIManager : MonoBehaviour
{
    [SerializeField] public Dash dash;
    [SerializeField] RawImage Dash0, Dash1, Dash2, Dash3, Dash4, Dash5, Dash6;
    // Start is called before the first frame update
    void Start()
    {
        Dash dash = gameObject.GetComponent<Dash>();
        Dash0.enabled = false;
        Dash1.enabled = false;
        Dash2.enabled = false;
        Dash3.enabled = false;
        Dash4.enabled = false;
        Dash5.enabled = false;
        Dash6.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        dash0();
        dash1();
        dash2();
        dash3();
        dash4();
        dash5();
        dash6();
    }

    void dash0()
    {
        if (dash.dashCharges == 0.0f) Dash0.enabled = true;
        else Dash0.enabled = false;
    }
    void dash1()
    {
        if (dash.dashCharges == 1.0f) Dash1.enabled = true;
        else Dash1.enabled = false;
    }
    void dash2()
    {
        if (dash.dashCharges == 2.0f) Dash2.enabled = true;
        else Dash2.enabled = false;
    }
    void dash3()
    {
        if (dash.dashCharges == 3.0f) Dash3.enabled = true;
        else Dash3.enabled = false;
    }
    void dash4()
    {
        if (dash.dashCharges == 4.0f) Dash4.enabled = true;
        else Dash4.enabled = false;
    }
    void dash5()
    {
        if (dash.dashCharges == 5.0f) Dash5.enabled = true;
        else Dash5.enabled = false;
    }
    void dash6()
    {
        if (dash.dashCharges == 6.0f) Dash6.enabled = true;
        else Dash6.enabled = false;
    }
}
