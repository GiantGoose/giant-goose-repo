using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MachinePartsNumber : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI UIText;
    public MachineConsole MC;
    private string mc;

    // Start is called before the first frame update
    void Start()
    {
        MachineConsole MC = gameObject.GetComponent<MachineConsole>();
        UIText.text = MC.MachineParts.ToString() + (" /3");
    }

    // Update is called once per frame
    void Update()
    {
        UIText.text = (MC.MachineParts.ToString() +(" /3"));
    }
}
