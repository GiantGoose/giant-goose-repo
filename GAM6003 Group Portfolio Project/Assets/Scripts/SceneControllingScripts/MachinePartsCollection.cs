using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MachinePartsCollection : MonoBehaviour
{
    bool playerEntered = false;
    public bool hasPart = false;
    public GameObject MachinePart;
    [SerializeField] private TextMeshProUGUI textInfo;

    void Update()
    {
        if (playerEntered == true)
        {
            PickUpInteract();
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            playerEntered = true;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            playerEntered = false;
        }
    }

    void PickUpInteract()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Player Collected ");
            hasPart = true;
            textInfo.text = null;
            MachinePart.SetActive(false);
            playerEntered = false;          
        }
    }
}
