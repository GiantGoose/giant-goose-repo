using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashChargeChanger : MonoBehaviour
{
    public float dashAmountReset;
    [SerializeField] private GameObject player;
    [SerializeField] private Dash dash;
    
    void Start()
    {
        player = GameObject.Find("Player");
        dash = player.GetComponent<Dash>();
    }

    void OnTriggerEnter(Collider coll)
    {
        Debug.Log("Player in collider");
        if (coll.gameObject.tag == "Player")
        {
            dash.dashCharges = dashAmountReset;              
            Debug.Log("DashChargesIncreased");
            dash.UpdateDashTextAmount();
        }
    }
}
