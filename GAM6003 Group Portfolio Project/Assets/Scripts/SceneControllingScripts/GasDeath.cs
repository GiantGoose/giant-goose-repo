using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GasDeath : MonoBehaviour
{
    [SerializeField] private float deathTimer;
    [SerializeField] private Transform deathWaypoint;
    private GameObject player;
    private PlayerDeathController playerDeathController;

    void Start()
    {
        player = GameObject.Find("Player");
        playerDeathController = player.GetComponent<PlayerDeathController>();
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player") Invoke("PlayerDies", deathTimer);
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Player") PlayerLives();
    }

    void PlayerDies() => playerDeathController.PlayerDies(deathWaypoint);

    void PlayerLives() => CancelInvoke(); 
}
