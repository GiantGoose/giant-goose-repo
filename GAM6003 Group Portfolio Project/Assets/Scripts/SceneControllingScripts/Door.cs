using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    bool DoorOpened = false;
    public Key key;
    [SerializeField] private float DoorHeight;
    [SerializeField] private float DoorTimer = 2.0f;
    [SerializeField] private Reset reset;
    [SerializeField] private int resetNumber;

    void Update()
    {
        if (DoorOpened == true)
        {
            Debug.Log("player in trigger");
            DoorOpen();
        }
    }

    void Start()
    {
        Key key = gameObject.GetComponent<Key>();

    }
    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            DoorOpened = true;
        }
    }
    private void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag =="Player")
        {
            DoorOpened = false;
        }
    }
    private void DoorOpen()
    {
        Debug.Log("door open void active");
        if (Input.GetKeyDown(KeyCode.E) && key.haveKey == true)
        {
            reset.roomNum = resetNumber;
            Debug.Log("player opened door with key");
            transform.Translate(0.0f, DoorHeight, 0.0f);
            Invoke("DoorClose", DoorTimer);
        }
    }
    void DoorClose()
    {
        transform.Translate(0.0f, -DoorHeight, 0.0f);
    }
}

