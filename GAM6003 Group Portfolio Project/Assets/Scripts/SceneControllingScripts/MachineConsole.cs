using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineConsole : MonoBehaviour
{
    [SerializeField] GameObject MachinePart1, MachinePart2, MachinePart3, rewindButton, waypointButton;
    [SerializeField] TimeReversal timeReversal;
    [SerializeField] Reset reset;
    public MachinePartsCollection mpc1, mpc2, mpc3;
    public int MachineParts;
    private bool playerEntered = false;
    public bool machineComplete;
    // Start is called before the first frame update
    void Start()
    {
        machineComplete = false;
        MachineParts = 0;
        MachinePart1.GetComponent<Renderer>().enabled = false;
        MachinePart2.GetComponent<Renderer>().enabled = false;
        MachinePart3.GetComponent<Renderer>().enabled = false;
        MachinePartsCollection mpc1 = gameObject.GetComponent<MachinePartsCollection>();
        MachinePartsCollection mpc2= gameObject.GetComponent<MachinePartsCollection>();
        MachinePartsCollection mpc3 = gameObject.GetComponent<MachinePartsCollection>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerEntered) MachinePartsShow();
        if (MachineParts == 3) machineComplete = true;
    }
    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            playerEntered = true;
        }
    }
    private void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            playerEntered = false;
        }
    }
    
    void MachinePartsShow()
    {
        if (Input.GetKeyDown("e") && mpc1.hasPart == true)
        {
            MachineParts++;
            MachinePart1.GetComponent<Renderer>().enabled = true;
            timeReversal.canReverse = true;
            rewindButton.SetActive(true);
            reset.roomNum = 3;
            mpc1.hasPart = false;
        }
        else if (Input.GetKeyDown("e") && mpc2.hasPart==true)
        {
            MachineParts++;
            MachinePart2.GetComponent<Renderer>().enabled = true;
            timeReversal.canSetWaypoint = true;
            waypointButton.SetActive(true);
            reset.roomNum = 4;
            mpc2.hasPart = false;
        }
        else if (Input.GetKeyDown("e") && mpc3.hasPart==true)
        {
            MachineParts++;
            MachinePart3.GetComponent<Renderer>().enabled = true;
            reset.roomNum = 5;
            mpc3.hasPart = false;
        }
    }
}
