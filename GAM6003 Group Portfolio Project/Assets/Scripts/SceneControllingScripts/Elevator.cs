using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    [SerializeField] private Transform elevator, floor1, floor2;
    [SerializeField] private float ElevatorSpeed = 0.02f;
    private bool PlayerHere = false;
    [SerializeField] private bool elevatorMoveUp = false;
    [SerializeField] private GameObject player; //Getting the player gameobject during runtime - Luke
    [SerializeField] private TimeReversal timeReversal; //Getting the active Time Reversal script during runtime - Luke
    [SerializeField] private bool playerInteracted;

    void Start()
    {
        player = GameObject.Find("Player");
        timeReversal = player.GetComponent<TimeReversal>();
    }

    private void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.tag == "Player") PlayerHere = true;
    }

    private void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Player") PlayerHere = false;
    }

    void Update()
    {
        if (PlayerHere) ElevatorMove();

        if (elevatorMoveUp) ElevatorMoveUp();
        else if (!elevatorMoveUp) ElevatorMoveDown();

        PlayerReversed();
    }

    void ElevatorMove()
    {       
        if (Input.GetKeyDown("e") && elevatorMoveUp == true)
        {
            Invoke("MoveUp", 0.05f);
            playerInteracted = true;
        }

        if (Input.GetKeyDown("e") && elevatorMoveUp == false)
        {
            Invoke("MoveDown", 0.05f);
            playerInteracted = true;
        }
    }

    void ElevatorMoveUp() => transform.position = Vector3.Lerp(elevator.position, floor2.position, ElevatorSpeed);

    void ElevatorMoveDown() => transform.position = Vector3.Lerp(elevator.position, floor1.position, ElevatorSpeed);

    void MoveUp() => elevatorMoveUp = false;

    void MoveDown() => elevatorMoveUp = true;

    void PlayerReversed()
    {
        if (timeReversal.isRewinding && playerInteracted)
        {
            if (elevatorMoveUp) Invoke("MoveUp", 0.05f);
            else if (!elevatorMoveUp) Invoke("MoveDown", 0.05f);
            playerInteracted = false;
        }
    }
}