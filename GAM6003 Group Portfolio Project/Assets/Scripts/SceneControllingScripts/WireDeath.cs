using UnityEngine;

public class WireDeath : MonoBehaviour
{
    [SerializeField] private Transform deathWaypoint;
    [SerializeField] private GameObject player;
    [SerializeField] private PlayerDeathController playerDeathController;

    void Start()
    {
        player = GameObject.Find("Player");
        playerDeathController = player.GetComponent<PlayerDeathController>();
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player") playerDeathController.PlayerDies(deathWaypoint);
    }
}
