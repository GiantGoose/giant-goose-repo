using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassPropulsion : MonoBehaviour
{
    [SerializeField] private Transform Shard, Home;
    [SerializeField] private float ShardSpeed = 0.02f, timer, ShardMoveTimer, force, StopMoveTimer;
    [SerializeField] private bool ForwardBackward = false, Sideways = false, Up = false;
    public bool launch = false;
    Rigidbody rb;
    bool GlassMove = false;
    Collider col;
    public GlassLauncher GL;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();

    }

    // Update is called once per frame
    void Update()
    {
        if (GlassMove) Reverse();
        if (GL.goTime)
        {
            Launch();
            Invoke("StopTime", ShardMoveTimer);
        }

        if (Input.GetKeyDown("p"))
        {
            Debug.Log("Button Pressed to launch");
            Launch();
        }
        else if (Input.GetKeyDown("o"))
        {
            rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePosition;
        }
        else if (Input.GetKeyDown("i"))
        {
            GlassMove = true;
            rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePosition;
            Invoke("Freeze", timer);
        }
    }

    void Launch()
    {
        Invoke("CollisionOff", 2.0f);
        rb.isKinematic = false;
        rb.constraints = RigidbodyConstraints.None;
        rb.useGravity = true;
        if (ForwardBackward == true)
        {
            rb.AddForce(transform.forward * force);
            Invoke("StopMoving", StopMoveTimer);
        }
        if (Sideways == true)
        {
            rb.AddForce(transform.right * force);
            Invoke("StopMoving", StopMoveTimer);
        }
        if (Up == true)
        {
            rb.AddForce(transform.up * force);
            Invoke("StopMoving", StopMoveTimer);
        }
        
    }
    void Reverse()
    {
        transform.position = Vector3.Lerp(Shard.position, Home.position, ShardSpeed);
        transform.rotation = Quaternion.Slerp(Shard.rotation, Home.rotation, ShardSpeed);
        col.enabled = false;
    }

    void StopTime()
    {
        GL.goTime = false;
    }
    void StopMoving()
    {
        rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePosition;
    }
    void Freeze()
    {
        GlassMove = false;
        col.enabled = true;
        rb.useGravity = false;
    }

    void CollisionOff()
    {
        col.enabled = false;
    }
}
