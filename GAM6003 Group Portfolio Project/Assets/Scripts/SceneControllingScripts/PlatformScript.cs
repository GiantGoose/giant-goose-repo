using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour
{  
    [SerializeField] private Transform Platform, Destination1, Destination2,Child; 
    [SerializeField] private float PlatformSpeed = 0.02f;
    bool PlayerHere = false;
    [SerializeField] private bool PlatformMoveTo = false;
    bool elevatorMoveDown = false;
    CharacterController controller;
    private Vector3 moveDirection;
    private float moveSpeed;

    void Start()
    {
        moveSpeed = PlatformSpeed;
    }
     // Update is called once per frame
    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            PlayerHere = true;
        }
    }

     private void OnTriggerExit(Collider coll)
     {
         if (coll.gameObject.tag == "Player")
         {
             PlayerHere = false;
         }
     }

     void Update()
     {
         if (PlayerHere)
         {
             PlatformMove();
         }

         if (PlatformMoveTo) PlatformMove1();
         else if (!PlatformMoveTo) PlatformMove2();
     }

     void PlatformMove()
     {
         if (Input.GetKeyDown("e") && PlatformMoveTo == true) Invoke("MoveTo", 0.05f);

         if (Input.GetKeyDown("e") && PlatformMoveTo == false) Invoke("MoveHome", 0.05f);
     }

     void PlatformMove2()
     {
        transform.position = Vector3.Lerp(Platform.position, Destination2.position, PlatformSpeed);
        Child.transform.SetParent(this.transform.parent);
    }

     void PlatformMove1()
     {
         transform.position = Vector3.Lerp(Platform.position, Destination1.position, PlatformSpeed);
         moveDirection = transform.forward * 6;
         controller.Move(moveDirection * Time.deltaTime * 6);
        Child.transform.SetParent(this.transform.parent);
    }

     void MoveTo()
     {
        PlatformMoveTo = false;
        Child.transform.parent = null;
     }

     void MoveHome()
     {
         PlatformMoveTo = true;
        Child.transform.parent = null;
    }
   }
