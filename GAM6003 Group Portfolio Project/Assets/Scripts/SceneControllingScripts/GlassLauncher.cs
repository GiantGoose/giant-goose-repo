using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassLauncher : MonoBehaviour
{
    public bool goTime = false;
    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            goTime = true;
            Invoke("Destroy", 1.0f);
        }
    }
    void Destroy()
    {
        Destroy(this);
    }
}
