using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticDoor : MonoBehaviour
{
    [SerializeField] private float DoorHeight, DoorTimer;
    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            DoorMove();
            Invoke("DoorClose", DoorTimer);
        }
    }
    void DoorMove()
    {
        transform.Translate(0.0f, DoorHeight,0.0f );
        
    }
    void DoorClose()
    {
        transform.Translate(0.0f, -DoorHeight, 0.0f);
    }
}
