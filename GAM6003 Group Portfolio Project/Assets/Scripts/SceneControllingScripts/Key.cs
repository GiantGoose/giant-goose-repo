using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Key : MonoBehaviour
{
    public bool haveKey = false;
    bool playerEntered = false;
    public GameObject key;
    [SerializeField] private TextMeshProUGUI textInfo;

    // Start is called before the first frame update
    void Update()
    {
        if (playerEntered == true)
        {
            PickUpInteract();
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            playerEntered = true;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            playerEntered = false;
        }
    }

    void PickUpInteract()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            haveKey = true;
            textInfo.text = null;
            key.SetActive(false);
            playerEntered = false;
            Debug.Log("Player Collected ");
        }
    }
}
