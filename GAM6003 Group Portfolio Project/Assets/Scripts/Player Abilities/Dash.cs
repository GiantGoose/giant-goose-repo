using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Dash : MonoBehaviour
{
    //[SerializeField]private Text UIText;
    [SerializeField] private TextMeshProUGUI UItext;
    //[SerializeField] private TMPro.TMP_Text UITEXT;
    [SerializeField]
    CharacterController controller;
    [SerializeField] private float CooldownLength;
    private float maxDashTime = 1.0f;
    private float currentDashTime;
    private Vector3 moveDirection;
    private float dashDistance = 10.0f;
    private float dashStoppingSpeed = 0.1f;
    private float dashSpeed = 6.0f;
    private string dashes;
    public float dashCharges = 6.0f;
    private bool cooldown = false;

    private void Start()
    {
        dashes = dashCharges.ToString();
        currentDashTime = maxDashTime;
        //UITEXT.text = "Charges: " + dashes;
        UItext.text = "Charges: " + dashCharges.ToString();
    }

    void Update()
    {
        DashActive();
        dashes = dashCharges.ToString();
    }

    public void UpdateDashTextAmount()
    {
        UItext.text = "Charges: " + dashCharges.ToString();
    }

    void DashActive()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift) && dashCharges != 0 && cooldown == false)
        {
            currentDashTime = 0;
            dashCharges --;
            //UITEXT.text = "Charges: " + dashes;
            UItext.text = "Charges: " + dashCharges.ToString();
            cooldown = true;
            Invoke("CooldownReset", CooldownLength);
        }

        if (currentDashTime < maxDashTime)
        {
            moveDirection = transform.forward * dashDistance;
            currentDashTime += dashStoppingSpeed;
        }
        else moveDirection = Vector3.zero;
        controller.Move(moveDirection * Time.deltaTime * dashSpeed); //taken from character script
    }

    void CooldownReset()
    {
        cooldown = false;
    }
}

