using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSpin : MonoBehaviour
{
    private enum SpinRotation { X, Y, Z};

    [SerializeField]
    private SpinRotation rotation;
    public float spinSpeed = 3f;

    // Update is called once per frame
    void Update()
    {
        switch (rotation)
        {
            case SpinRotation.X:
                transform.Rotate(spinSpeed * Time.deltaTime, 0f, 0f);
                break;

            case SpinRotation.Y:
                transform.Rotate(0f, spinSpeed * Time.deltaTime, 0f);
                break;

            case SpinRotation.Z:
                transform.Rotate(0f, 0f, spinSpeed * Time.deltaTime);
                break;
        }
    }
}
